importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/3.6.1/workbox-sw.js"
);
console.log("Hello from SW, whatup");

// workbox.routing.registerRoute(
//   /https:\/\/jsonplaceholder\.typicode\.com/,
//   workbox.strategies.networkFirst()
// );
workbox.precaching.precacheAndRoute([]);

/*
*   Background Sync Basic
*/
// const bgSyncPlugin = new workbox.backgroundSync.Plugin("myQueueName", {
//   maxRetentionTime: 24 * 60 // Retry for max of 24 Hours
// });
// workbox.routing.registerRoute(
//   /https:\/\/jsonplaceholder\.typicode\.com\/posts/,
//   workbox.strategies.networkOnly({
//     plugins: [bgSyncPlugin]
//   }),
//   "POST"
// );
// workbox.precaching.precacheAndRoute([]);

/*
*   Background Sync advanced
*/
// const queue = new workbox.backgroundSync.Queue("myQueueName");

// self.addEventListener("fetch", event => {
//   // Clone the request to ensure it's save to read when
//   // adding to the Queue.
//   const promiseChain = fetch(event.request.clone()).catch(err => {
//     return queue.addRequest(event.request);
//   });

//   event.waitUntil(promiseChain);
// });
// workbox.precaching.precacheAndRoute([]);
