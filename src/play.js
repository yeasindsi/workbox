(function() {
  console.log("A request will me made with in 3 Secs...");

  /*
  *Async function
  */
  setTimeout(function() {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then(res => res.json())
      .then(res => {
        console.log(res, "HHHHHHHHHHH");
      });
  }, 3000);

  /*
*   Save Post
*/
  $("#create-post").on("click", () => {
    $.ajax({
      url: "https://jsonplaceholder.typicode.com/posts",
      method: "POST",
      data: {
        userId: 1,
        title: "Some Title",
        body: "Some Body"
      },
      success: function(data) {
        console.log(data);
      }
    });
  });
})();
