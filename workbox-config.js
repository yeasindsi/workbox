module.exports = {
  globDirectory: ".",
  globPatterns: ["**/*.{html,css,js}"],
  swDest: "build/sw.js",
  swSrc: "src-sw.js"
};
