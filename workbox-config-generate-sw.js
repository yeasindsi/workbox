module.exports = {
  globDirectory: ".",
  globPatterns: ["**/*.{html,css,js}"],
  swDest: "build/sw.js",
  runtimeCaching: [
    {
      urlPattern: /https:\/\/jsonplaceholder\.typicode\.com/,
      handler: "networkFirst"
    }
  ]
};
